global find_word
extern string_equals

section .text

; rdi - указатель на введнную строку
; rsi - указатель на последний элемент словаря
; результат в rax - 0 - не найдено, указатель на строку - найдено
find_word:
    .loop:
        push rsi
        add rsi, 16
        call string_equals ;1 - равны, 0 - не равны
        pop rsi

        test rax, 1
        jnz .success

        mov rsi, [rsi] ;помещаем в rsi указатель на предыдущий элемент словаря

        test rsi, rsi
        jz .not_found

        jmp .loop
    .success:
        add rsi, 16
        mov rax, [rsi - 8]
        ret
    .not_found:
        xor rax, rax
        ret
